defmodule Issues.Mixfile do
  use Mix.Project

  def project do
    [
      app: :issues,
      version: "0.0.1",
      name: "Issues",
      elixir: "~> 1.7",
      escript: escript_config(),
      source_url: "https://github.com/adequateDeveloper/programming-elixir-book-ch13-issues",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: Coverex.Task],
      # test_coverage: [tool: ExCoveralls],
      # preferred_cli_env: [
      #    "coveralls": :test,
      #    "coveralls.detail": :test,
      #    "coveralls.post": :test,
      #    "coveralls.html": :test
      # ],
      deps: deps()
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
      extra_applications: [
        :logger
      ]
    ]

    # start these sub-applications
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:ex_doc, "~> 0.19"},

      # an HTTP client application
      {:httpoison, "~> 1.5"},

      # a json library - see: https://github.com/devinus/poison
      # note - a newer version does not work with this project
      {:poison, "~> 3.1"},

      # A static code analysis tool
      {:credo, "~> 1.0", only: [:dev, :test], runtime: false},

      # A code coverage tool
      # {:excoveralls, "~> 0.10", only: :test}
      {:coverex, "~> 1.5", only: :test}
    ]
  end

  defp escript_config do
    [main_module: Issues.CLI]
  end
end
