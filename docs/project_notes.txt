Project: Fetching Issues from GitHub
------------------------------------
This is a command line program.
You pass in a GitHub user name, project name and optional count.

Ways of running the program:
  From the issues project root folder:
    $ mix run -e 'Issues.CLI.main(["-h"])'
    $ mix run -e 'Issues.CLI.main(["--help"])'
    $ mix run -e 'Issues.CLI.main(["elixir-lang", "elixir"])'
    $ mix run -e 'Issues.CLI.main(["elixir-lang", "elixir", "2"])'
    $ ./issues elixir-lang elixir 3

  From iex:
    $ iex -S mix
    iex> Issues.CLI.main ["-h"]
    iex> Issues.CLI.main ["elixir-lang", "elixir"]
    iex> Issues.CLI.process {"elixir-lang", "elixir", 1}
    iex > Issues.GithubIssues.fetch "elixir-lang", "elixir"

Rebuilding the escript build:
  $  mix escript.build

Generating project documentation:
  $ mix docs
  Then copy the full path of ./doc/index.html to a browser for viewing

Generating project code coverage:
  $ mix test --cover

--------------------------------------------------------------------------------
Testing the HTTPoison service:
  $ iex -S mix
  iex> Issues.GithubIssues.fetch("elixir-lang", "elixir")
  See ./docs/example-httpoison-output.txt
--------------------------------------------------------------------------------

ExUnit
------
Test file names must end with _test

--------------------------------------------------------------------------------

finding built-in libraries:
  1: https://elixir-lang.org/docs.html   # for built-in libraries
  2: https://hexdocs.pm/elixir/          # for built-in libraries  
  3: http://erlang.org/doc/              # for Erlang libraries

  A curated list of cool Elixir stuff:
  https://github.com/h4cc/awesome-elixir

--------------------------------------------------------------------------------

versioning options:  iex> h Version

Debian systems sometimes don't get all the erlang packages. If so, then:
  $ sudo apt-get install erlang-src
  $ sudo apt-get install erlang-dev


$ mix deps

# run default mix task (mix run) before dropping into iex
$ iex -S mix
iex > Issues.GithubIssues.fetch("elixir-lang", "elixir")
iex > Issues.CLI.process {"elixir-lang", "elixir", 1}


Task: Make a Command-Line escript Executable
See pg 163
$ mix escript.build
$ ./issues elixir-lang elixir 3

mix test test/doc_test.exs


https://medium.com/@hoodsuphopeshigh/testing-in-elixir-chapter-3-the-outside-world-is-scary-96748c920d9b

Note: Often you will see Application.get_env/3 inside a module attribute (see github_issues.ex). 
The reason that we are wrapping it inside a function call and not using a module attribute is due 
to compile-time vs run-time. 
Module attributes are available at compile-time but run-time you will need to wrap it inside of a function.

  defp request do
    Application.get_env(:chapter_3, :github)
  end


$ iex -S mix
iex> :observer.start


VS Code IDE Configuration
-------------------------
debugging: https://medium.com/@abadon.gutierrez/elixir-development-with-visual-studio-code-16b923e82653

plugins:
  elixir:
    vscode-elixir
    ElixirLS

  markdown:
    markdownlint

  git:
    Git History    