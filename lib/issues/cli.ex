defmodule Issues.CLI do
  @moduledoc """
  Handle the command line parsing and the dispatch to
  the various functions that end up generating a
  table of the last _n_ issues in a github project
  """

  require Logger
  import Issues.TableFormatter, only: [print_table_for_columns: 2]

  # run examples from the issues project root folder:
  # $ mix run -e 'Issues.CLI.main(["-h"])'
  # $ mix run -e 'Issues.CLI.main(["--help"])'
  # $ mix run -e 'Issues.CLI.main(["elixir-lang", "elixir"])'
  # $ mix run -e 'Issues.CLI.main(["elixir-lang", "elixir", "2"])'
  # $ ./issues elixir-lang elixir 3

  @default_number_of_entries Application.get_env(:issues, :default_number_of_entries)

  # Entry point - required by Escript. See mix.exs
  # see: https://hexdocs.pm/mix/master/Mix.Tasks.Escript.Build.html#content

  @doc """
  Application entry point.
  """
  def main(argv) do
    argv
    |> parse_args
    |> process
  end

  @doc """
  `argv` can be -h or --help, which returns :help.
  Otherwise it is a github user name, project name, and (optionally)
  the number of entries to format.

  Returns a tuple of `{ user, project, count }`, or `:help` if help was given.
  """

  # Elixir comes bundled with an option-argument-parsing library
  # see: https://hexdocs.pm/elixir/OptionParser.html
  def parse_args(argv) do
    parse =
      OptionParser.parse(argv,
        switches: [help: :boolean],
        aliases: [h: :help]
      )

    case parse do
      {[help: true], _args, _invalid} -> :help
      {_parsed, [user, project, count], _invalid} -> {user, project, String.to_integer(count)}
      {_parsed, [user, project], _invalid} -> {user, project, @default_number_of_entries}
      _other -> :help
    end
  end

  @doc """
   Process requests for either the user asking for help, or where
   a user, project and count  data are supplied.
  """
  def process(:help) do
    IO.puts("""
    usage:  issues <user> <project> [ count | #{@default_number_of_entries} ]
    """)

    System.stop(0)
  end

  # see: https://github.com/edgurgel/httpoison
  # see: ./docs/example-httpoison-output.txt
  def process({user, project, count}) do
    Issues.GithubIssues.fetch(user, project)
    |> decode_response
    # GitHub returns a list of maps, where each map contains a GitHub issue.
    |> convert_to_list_of_maps
    |> sort_into_ascending_order
    |> Enum.take(count)
    |> print_table_for_columns(["number", "created_at", "title"])
  end

  def decode_response({:ok, body}), do: body

  def decode_response({:error, error}) do
    Logger.error("Error fetching from Github: #{error["message"]}")
    System.stop(2)
  end

  def convert_to_list_of_maps(list) do
    # list |> Enum.map(&Enum.into(&1, Map.new()))
    list |> Enum.map(&Enum.into(&1, %{}))
  end

  def sort_into_ascending_order(list_of_issues) do
    # Enum.sort(list_of_issues, fn i1, i2 -> i1["created_at"] <= i2["created_at"] end)
    Enum.sort(list_of_issues, &(&1["created_at"] <= &2["created_at"]))
  end
end
