defmodule Issues.GithubIssues do
  @moduledoc """
  Transform a GitHub user name and project into a data structure containing
  that project's issues.
  """
  require Logger

  # required by the GitHub API
  @user_agent [{"User-agent", "Elixir dave@pragprog.com"}]

  # pulled from config.exs
  @github_url Application.get_env(:issues, :github_url)

  def fetch(user, project) do
    Logger.info("Fetching user #{user}'s project '#{project}' ...")

    # "https://api.github.com/repos/elixir-lang/elixir/issues"
    build_url(user, project)

    # HTTPoison.get("https://api.github.com/repos/elixir-lang/elixir/issues", [{"User-agent", "Elixir dave@pragprog.com"}])
    |> HTTPoison.get(@user_agent)
    |> handle_response
  end

  def build_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end

  # pattern match on the crucial elements of the response
  # def handle_response({ :ok, %{status_code: 200, body: body} }), do: { :ok,    Poison.Parser.parse!(body) }
  # def handle_response({   _, %{status_code:   _, body: body} }), do: { :error, Poison.Parser.parse!(body) }

  # See: example httpoison outputs in ./docs
  # body is a string containing data encoded in JSON format
  # the JSON that GitHub returns is a list of maps, where each map contains
  # a GitHub issue.
  def handle_response({:ok, %{status_code: 200, body: body}}) do
    Logger.info("Successful response:")

    # function version will not be compiled into the code. see: config/config.exs
    Logger.debug(fn -> inspect(body) end)

    {:ok, Poison.Parser.parse!(body)}
  end

  def handle_response({_some_error, %{status_code: status, body: body}}) do
    Logger.error("Error #{status} returned")
    {:error, Poison.Parser.parse!(body)}
  end
end
