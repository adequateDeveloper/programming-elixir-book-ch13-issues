defmodule GithubIssuesTest do
  use ExUnit.Case, async: true
  import Issues.GithubIssues

  @github_url Application.get_env(:issues, :github_url)

  test "build_url/2 builds correct url" do
    user = "elixir-lang"
    project = "elixir"
    expected_url = "#{@github_url}/repos/#{user}/#{project}/issues"
    assert expected_url == build_url(user, project)
  end
end
