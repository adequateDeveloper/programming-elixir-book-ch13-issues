defmodule CliTest do
  use ExUnit.Case, async: true

  # import ExUnit.{CaptureIO, CaptureLog}
  import Issues.CLI

  @default_number_of_entries Application.get_env(:issues, :default_number_of_entries)

  test "parse_args/1 returns :help when passed -h, --help, or any invalid switch option" do
    assert parse_args(["-h", "anything"]) == :help
    assert parse_args(["--help", "anything"]) == :help
    assert parse_args(["--anything", "anything"]) == :help
  end

  test "parse_args/1 returns a three element tuple based on the passed in parameters" do
    assert parse_args(["user", "project", "99"]) == {"user", "project", 99}
  end

  test "parse_args/1 returns a default count if the count parameter is not provided" do
    assert parse_args(["user", "project"]) == {"user", "project", @default_number_of_entries}
  end

  test "sort_into_ascending_order/1 sorts correctly" do
    result = sort_into_ascending_order(fake_created_at_list(["c", "a", "b"]))
    issues = for issue <- result, do: issue["created_at"]
    assert issues == ~w{a b c}
  end

  # can't run because implementation calls System.stop(0)
  # test "process/1 with a :help parameter returns usage instructions" do
  #   fun = fn ->
  #     assert process(:help) == :ok
  #   end
  #   assert capture_io( fun ) == "usage:  issues <user> <project> [ count | 4 ]\n\n"
  # end

  test "decode_response/1 with {:ok, body} parameter returns unchanged body" do
    expected_body = "test body"
    assert expected_body == decode_response({:ok, expected_body})
  end

  # can't run because implementation calls System.stop(2)
  # test "decode_response/1 with {:error, error} parameter logs error message" do
  #   fun = fn ->
  #     test_error = %{"message" => "test message"}
  #     assert decode_response({:error, test_error}) == :ok
  #   end
  #   assert capture_log( fun ) =~ "Error fetching from Github: test message"
  # end

  defp fake_created_at_list(values) do
    data = for value <- values, do: [{"created_at", value}, {"other_data", "xxx"}]
    convert_to_list_of_maps(data)
  end
end
