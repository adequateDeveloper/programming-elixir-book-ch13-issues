[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

# Issues Tool

## A tool that downloads and lists the n oldest issues from a GitHub project

[Following chapter 13 of 'Programming Elixir >= 1.6' by Dave Thomas](https://pragprog.com/book/elixir16/programming-elixir-1-6)

## Features

* OptionParser for parsing command line options
* Use of Application.get_env/2 to set module attributes from config
* Use of System.stop/1
* Use of dependencies HTTPoison, Poison
* Generates an escript executable
* Testing:
* * use of capture_io, & capture_log assertions
* Credo static code analysis
* Coverex test coverage statistics